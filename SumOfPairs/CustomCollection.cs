﻿using System.Collections;
using System.Collections.Generic;


namespace SumOfPairs
{
    /// <summary>
    /// Коллекция с повторяющимеся элементами
    /// </summary>
    public class CustomCollection : IEnumerable<KeyValuePair<int, int>>
    {
        /// <summary>
        /// Key - число, Value - сколько раз встречается в коллекции
        /// </summary>
        private readonly Dictionary<int, int> _dictionary;

        public CustomCollection()
        {
            _dictionary = new Dictionary<int, int>();
        }

        public void Add(int key)
        {
            if (_dictionary.ContainsKey(key))
            {
                _dictionary[key]++;
            }
            else
            {
                _dictionary.Add(key, 1);
            }
        }

        public IEnumerator<KeyValuePair<int, int>> GetEnumerator()
        {
            return ((IEnumerable<KeyValuePair<int, int>>) _dictionary).GetEnumerator();
        }

        public bool TryGetValue(int key, out int value)
        {
            return _dictionary.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
