﻿using System;

namespace SumOfPairs
{
    class Program
    {
        static void Main(string[] args)
        {
            var X = 5;

            //Коллекция может хранить повторяющиеся элементы, причем пары, например, (1,4) и (4,1) считаются разными.
            var collection = new CustomCollection
            {
                1,
                4,
                4,
                4, 
                -1,
                6
            };

            foreach (var pair in collection)
            {
                int value;

                var otherKey = X - pair.Key;

                if (!collection.TryGetValue(otherKey, out value)) continue;

                for (var i = 0; i < value + pair.Value; i++)
                {
                    Console.WriteLine($"{pair.Key} and {otherKey}");
                }
            }
        }
    }
}
