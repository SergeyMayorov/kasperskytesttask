﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConcurrentQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            var queue = new ConcurrentQueue<int>();

            const int writerCount = 2;
            const int readerCount = 2;

            #region Writers

            for (var index = 0; index < writerCount; index++)
            {
                Task.Factory.StartNew(() =>
                {
                    for (var i = 0; i < 20; i++)
                    {
                        queue.Push(i);
                        Thread.Sleep(100);
                    }
                });
            }

            #endregion

            #region Readers
            for (var index = 0; index < readerCount; index++)
            {
                Task.Factory.StartNew(() =>
                {
                    for (var i = 0; i < 20; i++)
                    {
                        queue.Pop();

                        Thread.Sleep(1000);
                    }
                });
            }
            #endregion

            Console.ReadKey();

        }
    }
}
