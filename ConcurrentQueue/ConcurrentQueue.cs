﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConcurrentQueue
{
    public sealed class ConcurrentQueue<T>
    {
        private readonly Queue<T> _queue;
        private readonly object _locker;

        public ConcurrentQueue()
        {
            _locker = new object();
            _queue = new Queue<T>();
        }

        public void Push(T item)
        {
            lock (_locker)
            {    
                _queue.Enqueue(item);
                Console.WriteLine($"Item = {item} was wrote into queue by thread Id = {Thread.CurrentThread.ManagedThreadId}");
                Monitor.Pulse(_locker);
            }
        }

        public T Pop()
        {
            lock (_locker)
            {
                while (!_queue.Any())
                {
                    Monitor.Wait(_locker);
                }
                var item = _queue.Dequeue();
                Console.WriteLine($"\tItem = {item} was read from queue by thread Id = {Thread.CurrentThread.ManagedThreadId}");
                return item;
            }
        }

    }
}
